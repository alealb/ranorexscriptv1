﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace ScriptRanorexAutomation
{
    public class CTestSuite
    {
        public string TcSuiteName;
        public int TcSuiteNumber { get; set; }
        public int TSuiteNumber { get; set; }
        

        private FileInfo[] TestSuiteDirectoryMethod(String assemblyPath)
        {


            // C:/ Users / Alejandro.leal / Desktop / FINANCE_TEST / ScriptRanorexAutomation.exe
            string dirpath = assemblyPath;
            dirpath = assemblyPath.Replace("ScriptRanorexAutomation.exe", "bin/Debug");
            String mydir = dirpath;

            int fCount = Directory.GetFiles(mydir, "*.rxtst", SearchOption.AllDirectories).Length;

            DirectoryInfo d = new DirectoryInfo(dirpath);

            FileInfo[] Files = d.GetFiles("*.rxtst");

            return Files;
        }
        
        public List<CTestSuite> TestSuiteListMethod(CParameters assemblyParameter)
        {
            FileInfo[] Files = TestSuiteDirectoryMethod(assemblyParameter.AssemblyPath);

            List<CTestSuite> testSuites = new List<CTestSuite>();

            foreach (FileInfo file in Files)
            {
                CTestSuite suite = new CTestSuite
                {
                    TcSuiteName = file.Name,
                    TcSuiteNumber = TSuiteNumber
                };
                testSuites.Add(suite);
                TSuiteNumber++;
            }

            return testSuites;
        }

        public void PrintTestSuiteOptionMethod(List<CTestSuite> testsuites)
        {
            Console.WriteLine("---------------SUITES AVAILABLE---------------");
            foreach (var element in testsuites)
            {                
                Console.WriteLine("Option number {0} : {1}", element.TcSuiteNumber, element.TcSuiteName);
               
            }
            Console.WriteLine("----------------------------------------------");
        }

    }
}
