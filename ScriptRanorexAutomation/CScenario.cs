﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;
using System.Xml;
using System.IO;

namespace ScriptRanorexAutomation
{
    public class CScenario
    {
        public string ScenarioName { get; set; }
        public int ScenarioNumber { get; set; }
        public int ItScenarioNumber { get; set; }
        public string PathConcat { get; set; }
        private string _pathConcat;


        public List<CScenario> ListTestCasesMethod(CTestSuite testSuite, string projectPath ,string parent, string child)
        {
            List<CScenario> testCases = new List<CScenario>();

            XmlDocument doc = new XmlDocument();
            doc.Load(projectPath + testSuite.TcSuiteName);

            XmlNodeList xParent = doc.GetElementsByTagName(parent);
            XmlNodeList xChild = ((XmlElement)xParent[0]).GetElementsByTagName(child);
            Console.WriteLine("");
            Console.WriteLine("---------------LIST----------------");
            foreach (XmlElement nodo in xChild)
            {
                string xName = nodo.GetAttribute("name");
                string xReportLevel = nodo.GetAttribute("reportlevel");

                if (xReportLevel == "")
                {
                    CScenario testCase = new CScenario()
                    {
                        ScenarioName = xName,
                        ScenarioNumber = ItScenarioNumber
                    };
                    testCases.Add(testCase);
                    Console.WriteLine(ItScenarioNumber + ". "+ xName);
                    ItScenarioNumber++;
                }
            }
            return testCases;
        }
        private int SelectOption(List<CScenario> testScenarioList)
        {
            string selectedOption;
            int option;
            
            Console.WriteLine("Select a test case");
            do
            {
                selectedOption = Console.ReadLine();
                option = Int32.Parse(selectedOption);
                if (option < 0 || option >= testScenarioList.Count)
                {
                    Console.WriteLine("Please, select an available option:");
                }
            } while (option < 0 || option >= testScenarioList.Count);

            return option;
        }

        public string ExecuteOption(List<CScenario> testScenarioList, CTestSuite element, CParameters emailParam, string projectPath, string solutionExe, string suitePath, string executionType)
        {
            int option = SelectOption(testScenarioList);

            foreach (var scenario in testScenarioList)
            {
                if (scenario.ScenarioNumber == option)
                {   
                    _pathConcat = projectPath + solutionExe + " " + suitePath + element.TcSuiteName + " /pa:To=" + emailParam.EmailParameter + " /pa:Enviroment=" + emailParam.Enviroment + " /pa:URL=" + emailParam.EnviromentURL + " /"+executionType+":" + scenario.ScenarioName;
                    Console.WriteLine(_pathConcat);
                }
            }
            return _pathConcat;
        }
    }
}
