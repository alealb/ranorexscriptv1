﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace ScriptRanorexAutomation
{
    public class CParameters
    {
       public string EmailParameter { get; set; }
       public string Enviroment { get; set; }
       public string EnviromentURL { get; set; }
       private readonly string[] _ArrayEnv = { "bestseller-test01.sandbox.operations.dynamics.com", "performacebestone.sandbox.operations.dynamics.com" };
       private readonly string[] _ArrayURL = { "https://bestseller-test01.sandbox.operations.dynamics.com/?cmp=205", "https://performacebestone.sandbox.operations.dynamics.com" };
       public string AssemblyPath { get; set; }


       public CParameters()
       {
            EmailParameter = "automation.malaga@bestseller.com";
            AssemblyPath = (new System.Uri(Assembly.GetExecutingAssembly().CodeBase)).AbsolutePath;
            Enviroment = "bestseller-test01.sandbox.operations.dynamics.com";
            EnviromentURL = "https://bestseller-test01.sandbox.operations.dynamics.com/?cmp=205";
        }   
        
       public string ChangeEmailParameter()
       {
           Console.WriteLine("Enter the email");
           EmailParameter = Console.ReadLine();
           return EmailParameter;
       }

        public string ChangeEnviromentParameter()
        {
            int option;
            Console.Clear();
            Console.WriteLine("---------------MENU OPTIONS----------------");
            Console.WriteLine("Please specify what do you want to execute:");
            Console.WriteLine("1. Add a new enviroment");
            Console.WriteLine("2. Select one of the pre-defined enviroments");
            do
            {
                string selectedOption = Console.ReadLine();
                option = Int32.Parse(selectedOption);
                if (option < 1 || option > 3)
                {
                    Console.WriteLine("Please, select an available option:");
                }
            } while (option < 1 || option > 2);

            switch (option)
            {

                case 1:
                    Console.WriteLine("Enter the env path (Eg: bestseller-test01.sandbox.operations.dynamics.com ");
                    Enviroment = Console.ReadLine();
                    Console.WriteLine("Enter the env url (Eg: https://bestseller-test01.sandbox.operations.dynamics.com/?cmp=205 ");
                    EnviromentURL = Console.ReadLine();
                    break;
                case 2:
                    int envOption = 0;
                    Console.WriteLine("Select pre-define env");
                    foreach (var env in _ArrayEnv)
                    {

                        Console.WriteLine("Option {0} : {1} ", envOption, env);
                        envOption++;
                    }

                    do
                    {
                        string selectedOption = Console.ReadLine();
                        option = Int32.Parse(selectedOption);
                        if (option < 0 || option > 1)
                        {
                            Console.WriteLine("Please, select an available option:");
                        }
                        Enviroment = _ArrayEnv[option];
                        EnviromentURL = _ArrayURL[option];
                        
                    } while (option < 0 || option > 1);
                    break;
            }
            return Enviroment;
        }

        public void ListParametersDefault()
        {
            Console.WriteLine("-----------DEFAULT PARAMETERS-----------");
            Console.WriteLine("Email: "+EmailParameter);
            Console.WriteLine("Enviroment: " + Enviroment);
            Console.WriteLine();
            Console.WriteLine();
        }

        public string GetScenarioPath(string _xmlPath)
        {
            string path = AssemblyPath;
            string replacePath = path.Replace("ScriptRanorexAutomation.exe", "path.xml");

            XmlDocument doc = new XmlDocument();
            doc.Load(replacePath);

            XmlNodeList xParentPath = doc.GetElementsByTagName(_xmlPath);
            XmlNodeList xScenarioPath = ((XmlElement)xParentPath[0]).GetElementsByTagName("path");
            

            foreach (XmlElement nodo in xScenarioPath)
            {
                path = nodo.GetAttribute("name");
               
            }
            
            return path;
        } 
   }
}
