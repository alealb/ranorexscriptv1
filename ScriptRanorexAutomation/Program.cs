using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Diagnostics;
using ScriptRanorexAutomation;
using System.IO;


namespace ScriptRanorexAutomationConsoleTest
{
    class ConsoleTest
    {
        static void Main(string[] args)
        {
            bool exit = false;
            CParameters emailParam = new CParameters();
            do
            {
            emailParam.ListParametersDefault();
            //OBJETS DECLARATION 
            var RanorexExecution = new CConsoleP();
            CTestSuite testSuite = new CTestSuite();
            CScenario testCases = new CScenario();
            CParameters parameters = new CParameters();


            //VAR DECLARATION
            List<CTestSuite> testSuiteList = new List<CTestSuite>();
            List<CScenario> testScenarioList = new List<CScenario>();

            string selectedOption;
            int selectedNumber;
            string pathConcat;
            int option;


                //PATHS 
            string projectPath = parameters.GetScenarioPath("projectpath");
            string solutionExe = parameters.GetScenarioPath("solutionExe");
            string suitePath = parameters.GetScenarioPath("suitePath");
            
                //PRINT THE TEST SUITES AVAILABLE
                testSuiteList = testSuite.TestSuiteListMethod(emailParam);
                testSuite.PrintTestSuiteOptionMethod(testSuiteList);

                //SELECT THE TEST SUITE TO EXECUTE THE TEST
                Console.WriteLine("Please, select the suite: ");
                do
                {
                    selectedOption = Console.ReadLine();
                    option = Int32.Parse(selectedOption);

                    if (option < 0 || option >= testSuiteList.Count)
                    {
                        Console.WriteLine("Please, select an available option:");
                    }
                } while (option < 0 || option >= testSuiteList.Count);

                selectedNumber = Int32.Parse(selectedOption);

                //MENU
                foreach (var element in testSuiteList)
                {

                    if (element.TcSuiteNumber == selectedNumber)
                    {
                        Console.Clear();
                        /*Console.WriteLine("Note: PDF Report of the execution will be automatically sent to automation.malaga@bestseller.com by default");
                        Console.WriteLine("Select option 4 to change the email configuration");
                        Console.WriteLine("Press enter to continue...");
                        Console.ReadKey();*/
                        Console.Clear();
                        Console.WriteLine("---------------MENU OPTIONS----------------");
                        Console.WriteLine("Please specify what do you want to execute:");
                        Console.WriteLine("1. Execute the default " + element.TcSuiteName + " suite");
                        Console.WriteLine("2. List and execute only of the " + element.TcSuiteName + " test cases");
                        Console.WriteLine("3. Execute an specific run configuration");
                        Console.WriteLine("4. Email parameter");
                        Console.WriteLine("5. Enviroment parameter");
                        Console.WriteLine("6. Exit");
                        do
                        {
                            selectedOption = Console.ReadLine();
                            option = Int32.Parse(selectedOption);
                            if (option < 1 || option > 5)
                            {
                                Console.WriteLine("Please, select an available option:");
                            }
                        } while (option < 1 || option > 5);

                        switch (option)
                        {
                            case 1:
                                pathConcat = projectPath + solutionExe + suitePath + element.TcSuiteName;
                                RanorexExecution.CreateProcess(pathConcat);
                                Console.Clear();
                                break;
                            case 2:
                                string TypeTC = "tc"; 
                                testScenarioList = testCases.ListTestCasesMethod(element, projectPath, "flatlistofchildren", "testcase");
                                pathConcat = testCases.ExecuteOption(testScenarioList, element, emailParam, projectPath, solutionExe, suitePath,TypeTC);
                                RanorexExecution.CreateProcess(pathConcat);
                                Console.Clear();
                                break;
                            case 3:
                                string TypeRC = "rc";
                                testScenarioList = testCases.ListTestCasesMethod(element, projectPath, "testconfigurations", "testconfiguration");
                                pathConcat = testCases.ExecuteOption(testScenarioList, element, emailParam, projectPath, solutionExe, suitePath, TypeRC);
                                RanorexExecution.CreateProcess(pathConcat);
                                Console.Clear();
                                break;
                            case 4:
                                emailParam.ChangeEmailParameter();
                                Console.Clear();
                                break;
                            case 5:
                                emailParam.ChangeEnviromentParameter();
                                Console.Clear();
                                break;
                            case 6:
                                exit = true;
                                break;
                        }
                    }
                }
            } while (exit != true);
        }
    }
}