﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScriptRanorexAutomation
{
    public class CConsoleP
    {
        Process consoleProcess = new Process();


        public void CreateProcess(String inputParams)
        {

            consoleProcess.StartInfo.FileName = "cmd.exe"; //File need to be open

            consoleProcess.StartInfo.CreateNoWindow = true;

            consoleProcess.StartInfo.RedirectStandardInput = true;

            consoleProcess.StartInfo.RedirectStandardOutput = true;

            consoleProcess.StartInfo.UseShellExecute = false;

            consoleProcess.Start();

            consoleProcess.StandardInput.WriteLine(inputParams);

            consoleProcess.StandardInput.Flush();

            consoleProcess.StandardInput.Close();

            Console.WriteLine(consoleProcess.StandardOutput.ReadToEnd());
        }
    }
}
